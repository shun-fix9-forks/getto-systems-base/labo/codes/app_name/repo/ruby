require "grpc"
require "app_name/api/repo_server"

GRPC::RpcServer.new.tap{|server|
  server.add_http2_port "0.0.0.0:3020", :this_port_is_insecure
  server.handle GettoCodes::AppName::Api::RepoServer
  server.run_till_terminated_or_interrupted [ "SIGTERM" ]
}
