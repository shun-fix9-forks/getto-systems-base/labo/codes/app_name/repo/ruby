require "app_name/pb/repo_services_pb"

require "app_name/api/version"

module GettoCodes
  module AppName
    module Api
      class RepoServer < Pb::Repo::Service
        def version(req, _call)
          Pb::Repo::Version::Response.new number: VERSION
        end
      end
    end
  end
end
