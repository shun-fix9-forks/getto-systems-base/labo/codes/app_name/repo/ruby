# Generated by the protocol buffer compiler.  DO NOT EDIT!
# Source: app_name/pb/repo.proto for package 'getto_codes.app_name.pb'

require 'grpc'
require 'app_name/pb/repo_pb'

module GettoCodes
  module AppName
    module Pb
      module Repo
        class Service

          include GRPC::GenericService

          self.marshal_class_method = :encode
          self.unmarshal_class_method = :decode
          self.service_name = 'getto_codes.app_name.pb.Repo'

          rpc :Version, Repo::Version::Request, Repo::Version::Response
        end

        Stub = Service.rpc_stub_class
      end
    end
  end
end
